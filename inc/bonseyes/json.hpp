#pragma once
/**
 * NVISO CONFIDENTIAL
 *
 * Copyright (c) 2009- 2014 nViso SA. All Rights Reserved.
 *
 * The source code contained or described herein and all documents related to
 * the source code ("Material") is the confidential and proprietary information
 * owned by nViso or its suppliers or licensors.  Title to the  Material remains
 * with nViso Sarl or its suppliers and licensors. The Material contains trade
 * secrets and proprietary and confidential information of nViso or its
 * suppliers
 * and licensors. The Material is protected by worldwide copyright and trade
 * secret laws and treaty provisions. You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with nViso.
 *
 * NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */

#include <nlohmann/json.hpp>

#include <plog/Log.h>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace nviso {

using json = nlohmann::json;

template <typename T>
bool setValue(T &val, const json &j, const std::string &key) {
  auto it = j.find(key);
  if (it == j.end()) return false;
  try {
    val = *it;
    return true;
  } catch (std::exception &e) {
    LOGE << "JSON Exception: " << e.what();
    return false;
  }
}

inline std::string toString(const json &val) {
  std::ostringstream ss;
  ss << val;
  return ss.str();
}

inline std::vector<char> readFile(const std::string &fileName) {
  std::ifstream ifs(fileName.c_str(),
                    std::ios::in | std::ios::binary | std::ios::ate);
  if (not ifs.good()) return std::vector<char>();

  std::ifstream::pos_type fileSize = ifs.tellg();
  ifs.seekg(0, std::ios::beg);
  std::vector<char> bytes(fileSize);
  ifs.read(bytes.data(), fileSize);
  return bytes;
}

inline json readJsonString(const std::string &str) {
  if (str.empty()) {
    LOGE << "empty JSON string";
    return json();
  }
  try {
    return json::parse(str);
  } catch (std::exception &e) {
    LOGE << "JSON Exception: " << e.what();
    return json();
  }
}

inline std::string readTextFile(const std::string &fileName) {
  std::ifstream iFile(fileName);
  if (!iFile.is_open()) {
    LOGE << "cannot read JSON file " << fileName;
    return "";
  }
  std::stringstream buffer;
  buffer << iFile.rdbuf();
  return buffer.str();
}

inline json readJsonFile(const std::string &fileName) {
  return readJsonString(readTextFile(fileName));
}

inline bool writeJsonFile(const json &value, const std::string &fileName) {
  std::ofstream fs(fileName);
  fs << std::setw(4) << value << std::endl;
  return true;
}

} // namespace nviso
